FROM openjdk:8-jdk-alpine

COPY target/demo-0.0.1-SNAPSHOT.jar /demo.jar

#RUN java -jar demo-0.0.1-SNAPSHOT.jar 

CMD ["java", "-jar", "target/demo-0.0.1-SNAPSHOT.jar"]
